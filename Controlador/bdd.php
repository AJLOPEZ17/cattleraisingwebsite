<?php
class bdd
{
 private $cnn;
 public function __construct()
 {
  $this->cnn = new PDO('mysql:host=localhost;dbname=cattlesystem', "root", "");
 }
 public function ini_sesion($usu, $pass)
 {

  $ini_s = $this->cnn->prepare("SELECT * FROM `usuarios` where nombre_usuario = '$usu' and contrasena = '$pass'");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

/*-------------------------------------------------------------ENTRADAS------------------------------------------------------------------------------------------ */

 public function ver_entradas()
 {
  $ini_s = $this->cnn->prepare("SELECT
  ed.fecha_entrada AS fecha,
  ed.marca AS marca,
  CONCAT(ed.serie,'-',ed.id_entrada) AS folio,
  ed.sexo AS sexo,
  p.nombre_proveedor AS proveedor
  FROM entradas_detalladas ed
  INNER JOIN proveedores p
  ON p.id_proveedor = ed.id_proveedor");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function entrada_global($fecha, $piezas, $serie, $folio, $nombre_usuario)
 {
  $ent = $this->cnn->prepare("INSERT INTO `entradas` ( `fecha_entrada`, `piezas`, `serie`, `folio`,`nombre_usuario`) VALUES ('$fecha', $piezas, '$serie', $folio,'$nombre_usuario')");
  $ent->execute();
 }

 public function entrada_detallada($fecha, $nombre, $sexo, $serie, $lote, $id_proveedor, $marca, $cantidad, $id_entrada)
 {
  $entd = $this->cnn->prepare("INSERT INTO `entradas_detalladas`(`fecha_entrada`, `nombre`, `sexo`, `serie`, `lote`,  `id_proveedor`, `marca`, `cantidad`, `id_entrada`) VALUES ('$fecha','$nombre','$sexo','$serie','$lote',$id_proveedor,'$marca',$cantidad,$id_entrada)");
  $entd->execute();
 }

 public function getid_eg($fecha)
 {

  $getid = $this->cnn->prepare("SELECT `id_entrada`,`piezas` FROM `entradas` where fecha_entrada = '$fecha'");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

 public function up_folio($id)
 {
  $upf = $this->cnn->prepare("UPDATE `entradas` SET `folio`= $id WHERE `id_entrada` = $id");
  $upf->execute();
 }
 public function sumatotal_entradas($id_entrada)
 {

  $getid = $this->cnn->prepare("SELECT SUM(`costo`) AS costo FROM `entradas_detalladas` where `id_entrada` = $id_entrada");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_totalentradas($total, $id_entrada)
 {
  $entd = $this->cnn->prepare("UPDATE `entradas` SET  `total`='$total' where id_entrada=$id_entrada");
  $entd->execute();

 }

 public function entrada_inventario($entradas, $existencia, $id_producto)
 {
  $entd = $this->cnn->prepare("UPDATE `inventario` SET  `entradas`='$entradas',  `existencia`= '$existencia' where id_producto='$id_producto' ");
  $entd->execute();

 }
 public function entrada_inventariodetallado($fecha, $id_prod, $sexo, $lote, $cantidad, $marca, $id_proveedor)
 {
  $entd = $this->cnn->prepare("INSERT INTO `inventario_detallado` (`fecha_entrada`, `id_producto`, `sexo`, `lote`, `cantidad`, `marca`,`id_proveedor`) VALUES ('$fecha' ,$id_prod,'$sexo', '$lote',$cantidad, '$marca', $id_proveedor);");
  $entd->execute();
 }
 public function existencia($id_prod)
 {

  $getid = $this->cnn->prepare("SELECT SUM(`cantidad`) AS cantidad FROM `inventario_detallado` where `id_producto` = $id_prod");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function gettotal($id_entrada)
 {

  $getid = $this->cnn->prepare("SELECT * FROM `entradas` where id_entrada=$id_entrada LIMIT 1");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

/*-------------------------------------------------------------SALIDAS------------------------------------------------------------------------------------------ */

 public function getid_sg($fecha)
 {

  $getid = $this->cnn->prepare("SELECT * FROM `ventas` where fecha_venta = '$fecha'");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_foliosalida($id)
 {
  $upf = $this->cnn->prepare("UPDATE `ventas` SET `folio`= $id WHERE `id_venta` = $id");
  $upf->execute();
 }
 public function getactual_salida($id_venta)
 {

  $getid = $this->cnn->prepare("SELECT ventas_detalladas.nombre AS nombre_producto,ventas_detalladas.serial AS serial, ventas_detalladas.precio AS precio,clientes.nombre_cliente AS nombre_cliente FROM `ventas_detalladas` INNER JOIN `clientes` ON ventas_detalladas.id_cliente=clientes.id_cliente where ventas_detalladas.id_salida=$id_venta ORDER BY fecha_salida DESC  ");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function getultima_salida($id_venta)
 {
  $getid = $this->cnn->prepare("SELECT SUM(cantidad) as piezas, ventas_detalladas.serie as serie, ventas_detalladas.serial AS serie_prod, ventas.serie AS serie,ventas.folio AS folio,
                ventas_detalladas.nombre AS nombre_producto,ventas_detalladas.precio AS precio, ventas_detalladas.lote AS lote FROM `ventas_detalladas`  INNER JOIN `ventas` ON ventas_detalladas.id_salida = ventas.folio
                 where ventas_detalladas.id_salida =$id_venta GROUP BY ventas_detalladas.lote ORDER BY fecha_venta DESC");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function gettotalcliente_salida($id_venta)
 {

  $getid = $this->cnn->prepare("SELECT ventas.total AS total, clientes.nombre_cliente AS nombre_cliente FROM `ventas` INNER JOIN `clientes` ON ventas.id_cliente=clientes.id_cliente where id_venta = $id_venta");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_pagotiposaldo($pago, $saldo, $id)
 {
  $upf = $this->cnn->prepare("UPDATE `ventas` SET `pago`= $pago, `tipo_pago`='', `saldo`=$saldo WHERE `id_venta` = $id");
  $upf->execute();
 }
 public function ver_salidas()
 {

  $ini_s = $this->cnn->prepare("SELECT nombre, ventas_detalladas.id_salida as folio, ventas_detalladas.serie as serie, ventas_detalladas.fecha_salida as fecha, SUM(ventas_detalladas.cantidad) as piezas,
                clientes.nombre_cliente as cliente, sum(ventas_detalladas.precio) as total FROM `ventas_detalladas` INNER JOIN clientes on
                ventas_detalladas.id_cliente=clientes.id_cliente GROUP BY ventas_detalladas.lote Order by ventas_detalladas.fecha_salida asc");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

 public function salida_global($id_cliente, $piezas, $fecha, $serie, $folio, $total, $nombre_usuario)
 {
  $sal = $this->cnn->prepare("INSERT INTO `ventas`(`id_venta`, `id_cliente`, `piezas`, `fecha_venta`, `utilidad`, `saldo`, `status`, `serie`, `folio`, `total`,`pago`,`tipo_pago`,`nombre_usuario`) VALUES (NULL,$id_cliente,$piezas,'$fecha',0,0,0,'$serie',$folio,$total,0,0,'$nombre_usuario')");
  $sal->execute();
 }

 public function salida_detallada($fecha, $nombre, $serie, $lote, $serial, $cantidad, $precio, $id_cliente, $id_salida)
 {
  $entd = $this->cnn->prepare("INSERT INTO `ventas_detalladas`(`fecha_salida`, `nombre`, `serie`, `lote`,`serial`, `cantidad`, `precio`, `id_cliente`, `id_saldetalle`, `id_salida`)
                VALUES ('$fecha','$nombre','$serie','$lote','$serial',$cantidad,$precio,'$id_cliente',NULL, $id_salida)");
  $entd->execute();

 }
 public function sumatotal_salidas($id_salida)
 {

  $getid = $this->cnn->prepare("SELECT SUM(`precio`) AS precio FROM `ventas_detalladas` where `id_salida` = $id_salida");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_totalsalidas($total, $piezas, $id_salida)
 {
  $entd = $this->cnn->prepare("UPDATE `ventas` SET  `total`=$total, `piezas`=$piezas  where id_venta=$id_salida");
  $entd->execute();

 }

/*-------------------------------------------------------------INVENTARIO---------------------------------------------------------------------------------------- */

 public function salida_inventario($salidas, $existencia, $id_producto)
 {
  $entd = $this->cnn->prepare("UPDATE `inventario` SET  `salidas`='$salidas',  `existencia`= '$existencia' where id_producto='$id_producto' ");
  $entd->execute();

 }
 public function getpieza_salidas($id_producto, $marca)
 {

  $getid = $this->cnn->prepare("SELECT * FROM `inventario_detallado` where `id_producto`=$id_producto And cantidad=1 AND marca='$marca' ");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_cantidadpieza($id)
 {
  $upp = $this->cnn->prepare("UPDATE `inventario_detallado` SET `cantidad`= 0 WHERE `id` = $id");
  $upp->execute();
 }
 public function inventario($producto)
 {

  $num = $this->cnn->prepare("SELECT * FROM `inventario` INNER JOIN productos on productos.id=inventario.id_producto where nombre = '$producto'");
  $num->execute();
  $rtn = $num->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

/*-------------------------------------------------------------ABONOS---------------------------------------------------------------------------------------- */

 public function abonos($id_cliente, $id_venta)
 {
  $abono = $this->cnn->prepare("INSERT INTO `abonos_cliente`(`id_abono`, `id_cliente`, `id_venta`, `total`, `saldo`, `abono`,`fecha_abono`) VALUES (NULL,$id_cliente,$id_venta,0,0,0,'')");
  $abono->execute();
 }

 public function up_abonosts($total, $saldo, $id_venta)
 {
  $entd = $this->cnn->prepare("UPDATE `abonos_cliente` SET `total`=$total,`saldo`=$saldo WHERE id_venta=$id_venta ");
  $entd->execute();

 }

 public function get_toab($folio_venta)
 {

  $gettoab = $this->cnn->prepare("SELECT * FROM `abonos_cliente` where id_venta =$folio_venta");
  $gettoab->execute();
  $rtn = $gettoab->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

 public function up_abonosabs($saldo, $abono, $fecha, $folio_venta)
 {
  $entd = $this->cnn->prepare("UPDATE `abonos_cliente` SET `saldo`=$saldo,`abono`=$abono,`fecha_abono`='$fecha' WHERE id_venta=$folio_venta ");
  $entd->execute();

 }

/*-------------------------------------------------------------REMISIONES---------------------------------------------------------------------------------------- */

 public function getid_rem($fecha)
 {

  $getid = $this->cnn->prepare("SELECT * FROM `remisiones` where fecha_salida = '$fecha'");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_folioremision($id)
 {
  $upf = $this->cnn->prepare("UPDATE `remisiones` SET `folio`= $id WHERE `id_remision` = $id");
  $upf->execute();
 }
 public function remision_global($id_usuario, $piezas, $fecha, $serie, $folio, $total)
 {
  $sal = $this->cnn->prepare("INSERT INTO `remisiones`(`id_usuario`, `piezas`, `fecha_salida`,`serie`, `folio`) VALUES ($id_usuario,$piezas,'$fecha','$serie',$folio)");
  $sal->execute();
 }
 public function remision_detallada($fecha, $id_producto, $marca, $serie, $cantidad, $motivo, $sexo, $lote, $id_usuario, $id_remision)
 {
  $entd = $this->cnn->prepare("INSERT INTO `remisiones_detalladas`(`fecha_salida`, `id_producto`,`marca`, `serie`,`lote`, `cantidad`,`sexo`,`motivo`,`id_usuario`, `id_remision`) VALUES ('$fecha',$id_producto,'$marca','$serie','$lote',$cantidad,'$sexo','$motivo', $id_usuario, $id_remision)");
  $entd->execute();
 }

 public function sumatotal_remisiones($id_salida)
 {

  $getid = $this->cnn->prepare("SELECT SUM(`precio`) AS precio FROM `remisiones_detalladas` where `id_remision` = $id_salida");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function up_totalremisiones($total, $piezas, $id_salida)
 {
  $entd = $this->cnn->prepare("UPDATE `remisiones` SET `piezas`=$piezas, `total`=$total where id_remision=$id_salida");
  $entd->execute();

 }
 public function getactual_remision($id_remision)
 {

  $getid = $this->cnn->prepare("SELECT * FROM remisiones_detalladas where id_remision = $id_remision");
  $getid->execute();
  $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

/*-------------------------------------------------------------OTROS---------------------------------------------------------------------------------------- */

 public function clientes()
 {
  $upf = $this->cnn->prepare("SELECT * FROM `clientes`");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function proveedores()
 {
  $upf = $this->cnn->prepare("SELECT * FROM `proveedores`");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function getProveedor($nombre)
 {
  $upf = $this->cnn->prepare("SELECT * FROM `proveedores` WHERE nombre_proveedor = '$nombre'");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function productos()
 {
  $upf = $this->cnn->prepare("SELECT * FROM `productos`");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function ver_inventario()
 {

  $ini_s = $this->cnn->prepare("SELECT
  ind.marca AS marca,
  ind.sexo AS sexo,
  ind.cantidad AS existencia,
  p.nombre AS tipo
  FROM inventario_detallado ind
  INNER JOIN productos p
  ON p.id = ind.id_producto;");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function ver_inventario_detallado()
 {

  $ini_s = $this->cnn->prepare("SELECT productos.id_producto as id_producto, productos.nombre_comercial as nombre, SUM(inventario_detallado.cantidad) as piezas,inventario_detallado.caducidad as caducidad
                        FROM `inventario_detallado`
                        INNER JOIN productos on productos.id_producto=inventario_detallado.id_producto
                          GROUP BY inventario_detallado.lote Order by inventario_detallado.id_producto asc");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

 public function ver_productos()
 {

  $ini_s = $this->cnn->prepare("SELECT * FROM  productos");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function ver_clientes()
 {

  $ini_s = $this->cnn->prepare("SELECT * FROM  clientes");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function ver_remisiones()
 {

  $ini_s = $this->cnn->prepare("SELECT
  rd.fecha_salida AS fecha,
  rd.marca AS marca,
  CONCAT(rd.serie,'-',rd.id_remision) AS folio,
  rd.sexo AS sexo,
  p.nombre AS tipo,
  rd.motivo AS motivo
  FROM remisiones_detalladas rd
  INNER JOIN productos p
  ON p.id = rd.id_producto");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function ver_abonos()
 {

  $ini_s = $this->cnn->prepare("SELECT c.nombre_cliente AS nombre_cliente,v.serie AS serie,v.folio AS folio,ac.total AS total,ac.saldo AS saldo,ac.abono AS abono,ac.fecha_abono AS fecha FROM `abonos_cliente` ac INNER JOIN `clientes` c ON ac.id_cliente = c.id_cliente INNER JOIN `ventas` v ON ac.id_venta = v.id_venta");
  $ini_s->execute();
  $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }

 public function create_gastos($fecha, $descripcion, $importe)
 {
  $cgastos = $this->cnn->prepare("INSERT INTO `gastos`(`id`, `fecha`, `descripcion`, `importe`) VALUES (NULL,'$fecha','$descripcion',$importe)");
  $cgastos->execute();
 }
 public function create_proveedores($nombre)
 {
  $cp = $this->cnn->prepare("INSERT INTO `proveedores`(`nombre_proveedor`) VALUES ('$nombre')");
  $cp->execute();
 }
 public function create_clientes($nombre_cliente)
 {
  $cp = $this->cnn->prepare("INSERT INTO `clientes`(`id_cliente`, `nombre_cliente`) VALUES (NULL,'$nombre_cliente')");
  $cp->execute();
 }
 public function create_productos($nombre)
 {
  $cp = $this->cnn->prepare("INSERT INTO `productos`(`nombre` ) VALUES ('$nombre')");
  $cp->execute();
 }
 public function reporte($inicio, $final)
 {
  $upf = $this->cnn->prepare("SELECT ventas.fecha_venta as fecha, clientes.nombre_cliente as cliente, ventas.piezas as piezas, ventas.total as total, ventas.saldo as saldo  FROM `ventas` inner join `clientes` on ventas.id_cliente=clientes.id_cliente WHERE fecha_venta BETWEEN '$inicio 00:00:00' AND '$final 23:00:00'");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function reporte_detallado($inicio, $final)
 {
  $upf = $this->cnn->prepare("SELECT ventas_detalladas.fecha_salida as fecha, clientes.nombre_cliente as cliente, ventas_detalladas.nombre as producto, ventas_detalladas.serial as serie, ventas_detalladas.folio as folio, ventas_detalladas.precio as precio FROM `ventas_detalladas` inner join `clientes` on ventas_detalladas.id_cliente=clientes.id_cliente WHERE ventas_detalladas.fecha_salida BETWEEN '$inicio 00:00:00' AND '$final 23:00:00'");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function reportegastos($inicio, $final)
 {
  $upf = $this->cnn->prepare("SELECT * FROM gastos WHERE fecha BETWEEN '$inicio 00:00:00' AND '$final 23:00:00'");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function cuenta_cliente($id_cliente)
 {
  $upf = $this->cnn->prepare("SELECT * FROM abonos_cliente WHERE id_cliente=$id_cliente");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
 public function nombre_cliente($id_cliente)
 {
  $upf = $this->cnn->prepare("SELECT * FROM clientes WHERE id_cliente=$id_cliente");
  $upf->execute();
  $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
  return $rtn;
 }
}
