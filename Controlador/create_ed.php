<?php
session_start();
require 'bdd.php';

date_default_timezone_set('America/Mexico_City');
$n_piezas   = $_POST["n_piezas"];
$nombre     = $_POST["nombreprod"];
$proveedor  = $_POST["nombre-proveedor"];
$fecha      = date("Y-n-j H:i:s");
$marca      = $_POST["marca"];
$sexo       = $_POST["sexo"];
$lote       = $_POST["lote"];
$serie      = "AE";
$id_entrada = $_SESSION["ID_ENTRADA"];
$piezas     = $_SESSION["PIEZAS"]     = $piezas     = $_SESSION["PIEZAS"] - $n_piezas;
$cantidad   = 1;
$marca      = $_POST["marca"];

$db = new bdd;

$res = $db->inventario($nombre);
foreach ($res as $row) {
 $entradas    = $row['entradas'];
 $id_producto = $row['id_producto'];
}

$resp = $db->getProveedor($proveedor);

foreach ($resp as $row) {
 $id_proveedor = $row['id_proveedor'];
}

for ($i = 0; $i < $n_piezas; $i++) {
 $db->entrada_inventariodetallado($fecha, $id_producto, $sexo, $lote, $cantidad, $marca, $id_proveedor);
}

$ex = $db->existencia($id_producto);
foreach ($ex as $key) {
 $existencia = $key['cantidad'];
}
$new_entradas   = $entradas + $n_piezas;
$new_existencia = $existencia;

$db->entrada_inventario($new_entradas, $new_existencia, $id_producto);

for ($i = 0; $i < $n_piezas; $i++) {
 $db->entrada_detallada($fecha, $nombre, $sexo, $serie, $lote, $id_proveedor, $marca, $cantidad, $id_entrada);

}

header("Location: ../entradasdetalladas.php");
