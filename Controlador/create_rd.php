<?php
session_start();
require 'bdd.php';

date_default_timezone_set('America/Mexico_City');

$db     = new bdd;
$piezas = $_POST["piezas"];
$nombre = $_POST["nombreprod"];
$marca  = $_POST["marca"];
$motivo = $_POST["motivo"];

$res = $db->inventario($nombre);
foreach ($res as $row) {
 $salidas     = $row['salidas'];
 $id_producto = $row['id_producto'];
}

for ($i = 0; $i < $piezas; $i++) {
 $prod = $db->getpieza_salidas($id_producto, $marca);

 foreach ($prod as $key) {
  $cantidad1 = $key['cantidad'];
  $lote      = $key['lote'];
  $id_pieza  = $key['id'];
  $sexo      = $key['sexo'];
  $lote      = $key['lote'];
 }
 $db->up_cantidadpieza($id_pieza);
}
$ex = $db->existencia($id_producto);

foreach ($ex as $key1) {
 $existencia = $key1['cantidad'];
}

$new_salidas    = $salidas + $piezas;
$new_existencia = $existencia;

$db->salida_inventario($new_salidas, $new_existencia, $id_producto);

$fecha = date("Y-n-j H:i:s");

$serie       = "RM";
$cantidad    = 1;
$id_remision = $_SESSION["ID_REM"];

for ($i = 0; $i < $piezas; $i++) {
 $db->remision_detallada($fecha, $id_producto, $marca, $serie, $cantidad, $motivo, $sexo, $lote, $_SESSION["ID_USU"], $id_remision);
}

header("Location: ../remisionesdetalladas.php");
