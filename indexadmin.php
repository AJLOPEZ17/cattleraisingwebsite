<?php
session_start();
?>
<!DOCTYPE HTML>
<!--
	Alpha by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>iCattle Raising</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
	<!-- Header -->
		<?php require 'Controlador/bdd.php';
$bd = new bdd();
if (isset($_SESSION["tipo1"]) == true) {
 $fechaG = $_SESSION["UltimoAcceso"];
 $actual = date("Y-n-j H:i:s");
 $tiempo = (strtotime($actual) - strtotime($fechaG));
 if ($tiempo >= 900) {
  header('Location: Controlador/logOut.php');
 } else {
  $_SESSION["UltimoAcceso"] = $actual;
 }

 $bienvenida = 'Bienvenido: ' . $_SESSION["usr"] . '';
 ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">

					<nav id="nav">
						<ul>
							<li><a href="indexadmin.php">Inicio</a></li>
							<li>
								<a href="#" class="icon fa-angle-down">&Aacutereas</a>
								<ul>
									<li><a href="indexalmacen.php">Corrales</a></li>

								</ul>
							</li>
							<li>
								<a href="#" class="icon fa-angle-down">Reportes</a>
								<ul>
									<li><a href="reporte.php">Venta general</a></li>
									<li><a href="reportegastos.php">Gastos</a></li>
								</ul>
							</li>
							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>ADMINISTRADOR</h2>
					<p> <?php echo $bienvenida ?></p>
					<img src="images/logo.png" border="1" alt="" width="200" height="200">

				</section>

			<!-- Main -->

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">

					</ul>
					<ul class="copyright">
						<li>&copy; Todos los derechos reservados</li><li>Aldo, Copado y Julián</li>
					</ul>
				</footer>

		</div>
			<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

			  <?php
} else {
 header('Location: index.php');
 echo ('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
}
?>

	</body>
</html>
