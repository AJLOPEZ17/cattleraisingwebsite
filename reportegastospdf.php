<?php
session_start();
require('fpdf/fpdf.php');
require 'Controlador/bdd.php';
$bd = new bdd();

$inicio=$_POST["inicio"];
$final=$_POST["final"];


$reporte = $bd->reportegastos($inicio, $final);


class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    $inicio=$_POST["inicio"];
$final=$_POST["final"];
    // Logo
    $this->Image('images/logo.jpeg',10,10,33);
    // Arial bold 15

    $this->SetFont('Arial', '', 11);
    $this->Ln(5);
    $this->Cell(150, 10,'');
    $this->Cell(50, 10, 'Fecha: '.date('d-m-Y').'', 0);
    $this->Ln(30);
    $this->Cell(75, 30,'',0);
    $this->SetFont('Arial', '', 11);
    $this->Cell(50, 15,'Reporte de gastos del:',0);
    $this->Ln(6);
    $this->Cell(73, 15,'',0);
    $this->Cell(50, 15,$inicio.' al '.$final,0);
    $this->Cell(10, 15,'',0);

    $this->Ln(5);

    $this->Cell(65, 10,'');
    $this->SetFont('Arial','B',15);
    // Título


}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(23);

$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(20, 8, 'Gasto', 0);
$pdf->Cell(40, 8, 'Fecha', 0);
$pdf->Cell(40, 8, 'Descripcion', 0);
$pdf->Cell(30, 8, 'Importe', 0);

$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA


$item = 0;
$totaluni = 0;
$totaldis = 0;
$total=0;
foreach ($reporte as $datos){
    $item = $item+1;
    $total=$total+$datos['importe'];
    $pdf->Cell(10, 8, '', 0);
    $pdf->Cell(20, 8, $item, 0);
    $pdf->Cell(40, 8,$datos['fecha'], 0);
    $pdf->Cell(40, 8,$datos['descripcion'], 0);
    $pdf->Cell(30, 8, '$ '.$datos['importe'].'', 0);

    $pdf->Ln(15);
}
$pdf->Cell(10, 8, '', 0);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(20, 8, 'TOTAL:  ', 0);
$pdf->Cell(40, 8,'  $ '. $total.'', 0);


$pdf->Output();
?>
