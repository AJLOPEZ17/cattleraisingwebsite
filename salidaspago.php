<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Onconational GDL - Entradas de producto</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
	<?php require 'Controlador/bdd.php';
        $db = new bdd();
if (isset($_SESSION["tipo3"])==true || isset($_SESSION["tipo1"])==true || isset($_SESSION["tipo2"])==true){
	if (isset($_SESSION["ID_SALIDA"])==true){
    $fechaG=$_SESSION["UltimoAcceso"];
    $actual = date("Y-n-j H:i:s");
    $tiempo=(strtotime($actual)-strtotime($fechaG));
    if ($tiempo >=900) {
        header('Location: Controlador/logOut.php');
    }
    else{
        $_SESSION["UltimoAcceso"]=$actual;
    }

$venta=$db->getultima_salida($_SESSION["ID_SALIDA"]);
$tocl=$db->gettotalcliente_salida($_SESSION["ID_SALIDA"]);

$form='					<div class="12u">

							<!-- Table -->
								<section class="box">
									<h3>NOTA DE VENTA</h3>


									<div class="table-wrapper">
										<table>
											<thead>
												<tr>
													<th>PRODUCTO</th>
													<th>SERIE</th>
													<th>LOTE</th>
													<th>FOLIO DE VENTA</th>
													<th>PRECIO</th>
												</tr>
											</thead>';
				foreach ($venta as $key) {

$form=$form.'											<tbody>
												<tr>
													<td>'.$key["nombre_producto"].'</td>
													<td>'.$key["serie_prod"].'</td>
													<td>'.$key["lote"].'</td>
													<td>'.$key['serie'].'   -  '.+ $key['folio'].'</td>
													<td>'.$key["precio"].'</td>
												</tr></tbody>
											<tfoot>
												<tr>

												</tr>
											</tfoot>';
											}
$form=$form.'

										</table>
										<ul class="actions">
										<form action="ticket.php" method="post">
										<li><textarea name="notas" rows="4" cols="50" placeholder="Notas">
</textarea></li><br/><br/><br/>

										<li><button type="submit" class="button" value"Ticket">Ticket</button></li><br/><br/><br/>

										</form>

									</ul>

									<hr />
									</div>
									<br/>
									';
				foreach ($tocl as $key1) {
				$form=$form.'<h2>TOTAL: $ '.$key1["total"].'</h2>
				<br/>
				<h2>CLIENTE: '.$key1["nombre_cliente"].'</h2>
				<br/>';

				}


$form=$form.'<form class="form" id="pago" role="form"  name="pago" method="POST">
									<div class="row uniform 50%" >
											<div class="4u(mobilep)">
												<label>MONTO A PAGAR:</label>
												<br/><p>$<input type="text" name="pago" id="pago" value="" required="" placeholder="" /></p>
											</div>
										</div>
											<div class="3u 6u(narrower) 12u$(mobilep)" align="center">

												<br/>
												<div align="left">
												<button type="submit" class="button" name=btnPagar>Pagar</button>
												</div>
											</div>
										</div>

									</form>

									<hr />';



				if(isset($_POST['btnPagar'])){

				foreach ($tocl as $key2) {
				$total=$key2['total'];
				}
				$pago=$_POST['pago'];

				if ($total<$pago) {
					$cambio=$pago-$total;
					$new_pago=$pago-$cambio;
					$saldo=0;
					$db->up_pagotiposaldo($new_pago,$saldo,$_SESSION["ID_SALIDA"]);
					$db->up_abonosts($total,$saldo,$_SESSION["ID_SALIDA"]);
					 $form='
					 	<h2>CAMBIO: $ '.$cambio.'</h2><br/>
					 	<h2>SALDO: $ '.$saldo.'</h2><br/>
									<ul class="actions">

										<li><a href="indexventas.php" class="button alt">Terminar</a></li>

									</ul>
									<hr />';


				}
				else{
				$cambio=$total-$pago;
				$new_pago=$total-$cambio;
				$saldo=$cambio;
				$cambio=0;
				$db->up_pagotiposaldo($new_pago,$saldo,$_SESSION["ID_SALIDA"]);
				$db->up_abonosts($total,$saldo,$_SESSION["ID_SALIDA"]);

				$form='
					 	<h2>CAMBIO: $ '.$cambio.'</h2><br/>
					 	<h2>SALDO: $ '.$saldo.'</h2><br/>
									<ul class="actions">


										<li><a href="indexventas.php" class="button alt">Terminar</a></li>

									</ul>
									<hr />';
			}

			        }
    ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1><a href="indexventas.php">OnconationalGDL</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="indexventas.php">Inicio</a></li>

							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<section class="box">
				<header>
						<h2>Salidas de producto</h2>
					</header>

					<?php echo $form; ?>


								</section>


			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
				  <?php
        }}
        else{
             header('Location: index.php');
             echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
        }
        ?>

	</body>
</html>
