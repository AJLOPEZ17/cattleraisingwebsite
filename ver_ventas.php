<?php
session_start();
include 'menuadmin.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#ventas').DataTable( {
            "language": {
                 "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        } );
    } );
	</script>


</head>

<style>
	header{
		text-align: center;
		width: 100%;
		height: 200px;
		background: url(images/pic04.jpg);
		background-size: cover;      
	}
	a:link   
	{   
		text-decoration:none;   
	}   
</style>
</style>
<?php require 'Controlador/bdd.php';
$bd = new bdd();
$rtn = $bd->ver_ventas();

?>

<header >
	<h2 style="color: white;  line-height: 200px;">Ventas</h2>
</header>

<section class="box">
	<div class="row">
		<div class="6u">
			<h4>Ventas</h4>
		</div>
		<div class="6u" style="text-align: right;">
			<a href='controlador/createVenta.php'><img src="images/plus.png"></a>
		</div>

	</div>

	<div class="row">
		<div class="12u">
			<div class="table-wrapper">
				<table class="alt" id="ventas">
					<thead>
						<tr>
							<th>Cliente</th>
							<th>N° Rem</th>
							<th>Fecha</th>
							<th>Piezas</th>
							<th>Descripción</th>
							<th>Costo</th>
							<th>Precio</th>
							<th>Total</th>
							<th>Utilidad</th>
						</tr>
					</thead>
					<tbody>

						<?php
						foreach ($rtn as $row) { 
							echo ('<tr>');  
							echo('<td>'.$row['nombre_cliente'].'</td>');
							echo('<td>'.$row['n_rem'].'</td>');
							echo('<td>'.$row['fecha_venta'].'</td>');
							echo('<td>'.$row['piezas'].'</td>');
							echo('<td>'.$row['nombre_producto'].'</td>');
							echo('<td>'.$row['precio'].'</td>');
							echo('<td>'.$row['costo'].'</td>');
							echo('<td>'.$row['total'].'</td>');
							echo('<td>'.$row['utilidad'].'</td>');
							echo ('</tr>');
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>



</html>
