<?php
session_start();
require('fpdf/fpdf.php');
require 'Controlador/bdd.php';
$bd = new bdd();

$rtn = $bd->ver_salidas();

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('images/logo.jpeg',10,10,33);
    // Arial bold 15
    $this->Cell(150, 30,'',0);
    $this->SetFont('Arial', '', 11);
    $this->Ln(10);
     $this->Cell(150, 10,'');
    $this->Cell(50, 10, 'Fecha: '.date('d-m-Y').'', 0);
    $this->Ln(10);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(20);
$pdf->Cell(72, 0, '', 0);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(20, 0, 'Reporte de Ventas', 0);
$pdf->Ln(15);
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(5, 8, '', 0);
$pdf->Cell(5, 8, 'No.', 0);
$pdf->Cell(35, 8, 'Fecha', 0);
$pdf->Cell(35, 8, 'Nombre', 0);
$pdf->Cell(30, 8, 'Cliente', 0);
$pdf->Cell(15, 8, 'Serie', 0);
$pdf->Cell(15, 8, 'Folio', 0);
$pdf->Cell(20, 8, 'Piezas', 0);
$pdf->Cell(20, 8, 'Precio c/u', 0);
$pdf->Cell(20, 8, 'Total', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA


$item = 0;
foreach ($rtn as $productos){
    $item = $item+1;
    $preciopp=$productos['total']/$productos['piezas'];

    $pdf->Cell(5, 8, '', 0);
    $pdf->Cell(5, 8, $item, 0);
    $pdf->Cell(35, 8,$productos['fecha'], 0);
    $pdf->Cell(35, 8,$productos['nombre'], 0);
    $pdf->Cell(30, 8,$productos['cliente'], 0);
    $pdf->Cell(15, 8,$productos['serie'], 0);
    $pdf->Cell(15, 8,$productos['folio'], 0);
    $pdf->Cell(20, 8,$productos['piezas'], 0);
    $pdf->Cell(20, 8,'$'.round( $preciopp,2), 0);
    $pdf->Cell(20, 8,'$'.$productos['total'].'', 0);
    $pdf->Ln(8);

}
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115,8,'',0);


$pdf->Output();
?>
