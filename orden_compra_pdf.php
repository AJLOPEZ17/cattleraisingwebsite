<?php
session_start();
require('fpdf/fpdf.php');
require 'Controlador/bdd.php';
$bd = new bdd();

$notas=$_POST["notas"];
$proveedor=$_POST["nombre_proveedor"];
$medicamento=$_POST["nombreprod"];

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('images/logo.jpeg',10,10,33);
    // Arial bold 15
    $this->Cell(150, 30,'',0);
    $this->SetFont('Arial', '', 11);
    $this->Ln(10);
     $this->Cell(150, 10,'');
    $this->Cell(50, 10, 'Fecha: '.date('d-m-Y').'', 0);
    $this->Ln(30);



    $this->Cell(80, 10,'');
    $this->SetFont('Arial','B',15);
    // Título
    $bd = new bdd();

    $tocl=$bd->gettotalcliente_salida($_SESSION["ID_SALIDA"]);
    foreach ($tocl as $cliente) {
           $cliente1=$cliente["nombre_cliente"];
        }

      $this->Cell('',10,$cliente1);

    $this->Ln(8);


}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(23);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(10, 8, 'No.', 0);
$pdf->Cell(60, 8, 'Nombre', 0);
$pdf->Cell(20, 8, 'Serial', 0);
$pdf->Cell(20, 8, 'Serie', 0);
$pdf->Cell(20, 8, 'Piezas', 0);
$pdf->Cell(20, 8, 'Precio c/u', 0);
$pdf->Cell(20, 8, 'Precio', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA

$productos = $bd->getultima_salida($_SESSION["ID_SALIDA"]);

$item = 0;
$totaluni = 0;
$totaldis = 0;
foreach ($productos as $productos2){
    $preciototal=$productos2['piezas']*$productos2['precio'];
    $item = $item+1;
    $totaluni = $totaluni + 1;
    $totaldis = $totaldis + 1;
    $pdf->Cell(10, 8, '', 0);
    $pdf->Cell(10, 8, $item, 0);
    $pdf->Cell(60, 8,$productos2['nombre_producto'], 0);
    $pdf->Cell(20, 8, 'AS', 0);
    $pdf->Cell(20, 8, $productos2['serie'], 0);
    $pdf->Cell(20, 8, $productos2['piezas'], 0);
    $pdf->Cell(20, 8, '$ '.$productos2['precio'].'.00', 0);
    $pdf->Cell(20, 8, '$ '.$preciototal.'.00', 0);
    $pdf->Ln(8);
}
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10,8,'',0);
$pdf->Cell(10,8,'',0);
$pdf->Cell(20,8,'',0);
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115,8,'',0);

$pdf->Cell(47,8,'Total:',0);
$pdf->Cell(20,8,'$'.$suma[0]['precio'].'',0);

$pdf->Ln(30);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(115,8,'NOTA:',0);
$pdf->Ln(10);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(115,8,$notas,0);

$pdf->Output();

?>
