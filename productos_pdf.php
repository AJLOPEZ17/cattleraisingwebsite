<?php
session_start();
require 'fpdf/fpdf.php';
require 'Controlador/bdd.php';
$bd = new bdd();

$rtn = $bd->ver_productos();

class PDF extends FPDF
{
// Cabecera de página
 public function Header()
 {
  // Logo
  $this->Image('images/logo.png', 10, 10, 33);
  // Arial bold 15
  $this->Cell(150, 30, '', 0);
  $this->SetFont('Arial', '', 11);
  $this->Ln(10);
  $this->Cell(150, 10, '');
  $this->Cell(50, 10, 'Fecha: ' . date('d-m-Y') . '', 0);
  $this->Ln(10);
 }

// Pie de página
 public function Footer()
 {
  // Posición: a 1,5 cm del final
  $this->SetY(-15);
  // Arial italic 8
  $this->SetFont('Arial', 'I', 8);
  // Número de página
  $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
 }
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(20);
$pdf->Cell(72, 0, '', 0);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(20, 0, 'Lista de Productos', 0);
$pdf->Ln(15);
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(20, 8, 'No.', 0);
$pdf->Cell(50, 8, 'Tipo de ganado', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA

$item = 0;
foreach ($rtn as $productos) {
 $item = $item + 1;

 $pdf->Cell(10, 8, '', 0);
 $pdf->Cell(20, 8, $item, 0);
 $pdf->Cell(52, 8, $productos['nombre'], 0);
 $pdf->Ln(8);
}
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115, 8, '', 0);

$pdf->Output();
