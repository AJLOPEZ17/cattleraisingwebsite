<?php
session_start();
?>
<!DOCTYPE HTML>
<!--
	Alpha by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Onconational GDL</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="landing">
	<!-- Header -->
		<?php require 'Controlador/bdd.php';
        $bd = new bdd();
if (isset($_SESSION["tipo3"])==true || isset($_SESSION["tipo1"])==true){
    $fechaG=$_SESSION["UltimoAcceso"];
    $actual = date("Y-n-j H:i:s");
    $tiempo=(strtotime($actual)-strtotime($fechaG));
    if ($tiempo >=900) {
        header('Location: Controlador/logOut.php');
    }
    else{
        $_SESSION["UltimoAcceso"]=$actual;
    }

    $bienvenida= 'Bienvenido a OnconationalGDL  '.$_SESSION["usr"].'' ;
    ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header" class="alt">
					<h1><a href="indexventas.php">OnconationalGDL</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="indexventas.php">Inicio</a></li>
							<li>
								<a href="#" class="icon fa-angle-down">Movimientos</a>
								<ul>
									<li><a href="salidas.php">Venta</a></li>
									<li><a href="orden_compra.php">Orden de Compra</a></li>

								</ul>
							</li>
							<li>
								<a href="#" class="icon fa-angle-down">Consultas</a>
								<ul>
									<li><a href="ver_salidas.php">Ventas</a></li>
									<li><a href="ver_remisiones.php">Remisiones</a></li>
									<li><a href="ver_productos.php">Productos</a></li>
									<li><a href="ver_inventario.php">Inventario</a></li>
									<li><a href="ver_clientes.php">Clientes</a></li>


								</ul>
							</li>
							<li>
								<a href="#" class="icon fa-angle-down">Ingresar</a>
								<ul>
									<li><a href="proveedores.php">Proveedores</a></li>
									<li><a href="Clientes.php">Clientes</a></li>



								</ul>
							</li>
							<li><a href="indexadmin.php">Administrador</a></li>
							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Banner -->
				<section id="banner">
					<h2>VENTAS</h2>
					<p> <?php echo $bienvenida?></p>

				</section>

			<!-- Main -->

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			  <?php
        }
        else{
             header('Location: index.php');
             echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
        }
        ?>

	</body>
</html>
