<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>iCattle Raising</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body >

	<?php
require 'Controlador/bdd.php';
$db = new bdd();
if (isset($_SESSION["tipo2"]) == true || isset($_SESSION["tipo1"]) == true) {
 if (isset($_SESSION["ID_REM"]) == true) {
  $fechaG             = $_SESSION["UltimoAcceso"];
  $actual             = date("Y-n-j H:i:s");
  $tiempo             = (strtotime($actual) - strtotime($fechaG));
  $_SESSION["PIEZAS"] = 0;
  if ($tiempo >= 900) {
   header('Location: Controlador/logOut.php');
  } else {
   $_SESSION["UltimoAcceso"] = $actual;

  }

  ?>
		<div id="page-wrapper">

			<!-- Header -->
			<header id="header">
				<h1><a href="indexalmacen.php"><img src="images/logovaca.png" border="1" alt="" width="50" height="45"></a></h1>
				<nav id="nav">
					<ul>
						<li><a href="indexalmacen.php">Inicio</a></li>

						<li><a href="Controlador/logOut.php">Cerrar sesi&oacuten</a></li>
					</ul>
				</nav>
			</header>

			<!-- Main -->
			<div id="main" class="container">
			<section class="box">
				<header>
					<h2>Salidas de ganado</h2>
				</header>

				<form class="form" id="salidadetallada" role="form"  name="salidadetallada" action="Controlador/create_rd.php" method="POST">
				<div class="row uniform 50%" >
						<div class="4u(mobilep)">
							<input type="hidden" name="piezas" id="piezas" value="1" />
						</div>
					</div>
					<div class="row uniform 50%" >
						<div class="4u(mobilep)">
							<input type="text" name="marca" id="marca" value="" required="" placeholder="Marca en la res" />
						</div>
					</div>
					<br>
					<div class="row uniform 50%" >
						<div class="6u(mobilep)">
						Tipo de ganado:
							<div class="select-wrapper">
								<select name="nombreprod">
									<?php
$rtn = $db->productos();
  foreach ($rtn as $row) {
   echo '<option value="' . $row['nombre'] . '">' . $row['nombre'] . '</option>';
  }
  ?>
								</select>
						</div>
					</div>
					<div class="row uniform 50%" >
					<div class="6u(mobilep)">
						Motivo:
							<div class="select-wrapper">
								<select name="motivo">
									<option value="MUERTE">MUERTE</option>
									<option value="VENTA">VENTA</option>
									<option value="CARNE">CARNE</option>
									<option value="JUBILACIÓN">JUBILACIÓN</option>
								</select>
						</div>
					</div>
					</div>
					</div>

					<br>
					<div class="row">
						<div class="3u 6u(narrower) 12u$(mobilep)" align="center">
							<div align="left">
								<button type="submit" class="button" name="btnIngresar" >Agregar</button>
							</div>
						</div>
					</div>
				</form>

				<hr />
				<?php
$rem  = $db->getactual_remision($_SESSION["ID_REM"]);
  $size = sizeof($rem);
  if ($size == 0) {
   $form = '<h2>Aún no agrega ningún producto a la remisión</h2>';
  } else {

   $form = '					<div class="12u">

							<!-- Table -->

									<h3>PRODUCTOS AGREGADOS</h3>


									<div class="table-wrapper" align="right">
										<table>
											<thead>
												<tr>
													<th>MARCA</th>
													<th>SEXO</th>
													<th>MOTIVO</th>
												</tr>
											</thead><tbody>';
   foreach ($rem as $key) {

    $form = $form . '
												<tr>
													<td>' . $key["marca"] . '</td>
													<td>' . $key["sexo"] . '</td>
													<td>' . $key["motivo"] . '</td>
												</tr>';
   }
   $form = $form . '
											</tbody>
											<tfoot>
												<tr>

												</tr>
											</tfoot>
										</table>
									</div>';

  }
  ?>
				<div class="box" align="right">
				<?php echo $form; ?>
				</div>

				<form name="form" >
  <input type="submit" class="button special" name="terminar" value="Terminar" />
</form>

<?php
if (isset($_REQUEST['terminar'])) {

   header("location: indexalmacen.php");
  }
  ?>

			</section>
			<div id="main" class="container">

			<!-- Footer -->
			<footer id="footer">
				<ul class="icons">

				</ul>
				<ul class="copyright">
					<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</footer>

		</div>

		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrollgress.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/util.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
		<script src="assets/js/main.js"></script>
<?php
}
} else {
 header('Location: index.php');
 echo ('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
}
?>
</body>
</html>
