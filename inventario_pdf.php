<?php
session_start();
require 'fpdf/fpdf.php';
require 'Controlador/bdd.php';
$bd = new bdd();

$rtn = $bd->ver_inventario();

class PDF extends FPDF
{
// Cabecera de página
 public function Header()
 {
  // Logo
  $this->Image('images/logo.png', 10, 10, 33);
  // Arial bold 15
  $this->Cell(150, 30, '', 0);
  $this->SetFont('Arial', '', 11);
  $this->Ln(10);
  $this->Cell(150, 10, '');
  $this->Cell(50, 10, 'Fecha: ' . date('d-m-Y') . '', 0);
  $this->Ln(10);
 }

// Pie de página
 public function Footer()
 {
  // Posición: a 1,5 cm del final
  $this->SetY(-15);
  // Arial italic 8
  $this->SetFont('Arial', 'I', 8);
  // Número de página
  $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
 }
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(20);
$pdf->Cell(72, 0, '', 0);
$pdf->SetFont('Arial', 'B', 15);
$pdf->Cell(20, 0, 'Inventario', 0);
$pdf->Ln(15);
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(5, 8, '', 0);
$pdf->Cell(15, 8, 'Marca', 0);
$pdf->Cell(40, 8, 'Sexo', 0);
$pdf->Cell(55, 8, 'Existencia', 0);
$pdf->Cell(75, 8, 'Tipo', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA

foreach ($rtn as $productos) {

 $pdf->Cell(5, 8, '', 0);
 $pdf->Cell(15, 8, $productos['marca'], 0);
 $pdf->Cell(40, 8, $productos['sexo'], 0);
 $pdf->Cell(55, 8, $productos['existencia'], 0);
 $pdf->Cell(75, 8, $productos['tipo'], 0);
 $pdf->Ln(8);
}

$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115, 8, '', 0);

$pdf->Output();
