<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>iCattle Raising</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
	<?php require 'Controlador/bdd.php';
$db = new bdd();
if (isset($_SESSION["tipo2"]) == true || isset($_SESSION["tipo1"]) == true) {
 $fechaG = $_SESSION["UltimoAcceso"];
 $actual = date("Y-n-j H:i:s");
 $tiempo = (strtotime($actual) - strtotime($fechaG));
 if ($tiempo >= 900) {
  header('Location: Controlador/logOut.php');
 } else {
  $_SESSION["UltimoAcceso"] = $actual;
 }

 $form = '					<form class="form" id="entradaglobal" role="form"  name="entradaglobal" method="POST">

										<div class="row uniform 50%" >
											<div class="4u(mobilep)">
												<input type="text" name="piezas" id="piezas" value="" required="" placeholder="No. de reses" />
											</div>
										</div>
										<div class="row">

											<div class="3u 6u(narrower) 12u$(mobilep)" align="center">

												<br/>
												<div align="left">
												<button type="submit" class="button" name=btnIngresar>Ingresar</button>
												</div>
											</div>
										</div>

									</form>

									<hr />';

 if (isset($_POST['btnIngresar'])) {
  date_default_timezone_set('America/Mexico_City');
  $fecha          = date("Y-n-j H:i:s");
  $piezas         = $_POST["piezas"];
  $serie          = "AE";
  $folio          = 0;
  $nombre_usuario = $_SESSION["usr"];
  $db->entrada_global($fecha, $piezas, $serie, $folio, $nombre_usuario);
  $get_id = $db->getid_eg($fecha);
  $size   = sizeof($get_id);
  if ($size == 0) {
  } else {
   foreach ($get_id as $key) {
    $_SESSION["ID_ENTRADA"] = $key['id_entrada'];
    $_SESSION["PIEZAS"]     = $key['piezas'];
   }
   $id   = $_SESSION["ID_ENTRADA"];
   $rntm = $db->up_folio($id);

  }
  $form = '
									<ul class="actions">

										<li><a href="entradasdetalladas.php" class="button alt">Siguiente</a></li>
									</ul>



									<hr />';

 }

 ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1><a href="indexalmacen.php"><img src="images/logovaca.png" border="1" alt="" width="50" height="45"></a></h1>
					<nav id="nav">
						<ul>
							<li><a href="indexalmacen.php">Inicio</a></li>

							<li><a href="Controlador/logOut.php">Cerrar sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
			<div id="main" class="container">
				<section class="box">
				<header>
						<h2>Entradas de ganado</h2>
					</header>

					<?php echo $form; ?>


								</section>
								</div>

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
					</ul>
					<ul class="copyright">
					<li>&copy; Todos los derechos reservados</li><li>Aldo, Copado y Julián</li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
				  <?php
} else {
 header('Location: index.php');
 echo ('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
}
?>

	</body>
</html>

