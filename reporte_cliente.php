<?php
session_start();
include 'menuadmin.php';
require 'Controlador/bdd.php';
$bd = new bdd();
if (isset($_POST["cliente"])) {
    $cliente=$_POST["cliente"];
    $res = $bd->cuenta_cliente($cliente);
}else{
  $cliente=0;
  $res = $bd->cuenta_cliente($cliente);
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#salidas tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );

    // DataTable
    var table = $('#salidas').DataTable({
            "language": {
                 "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        } );

    // Apply the search
    table.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
	</script>


</head>

<style>
	header{
		text-align: center;
		width: 100%;
		height: 200px;
		background: url(images/pic04.jpg);
		background-size: cover;
	}
	a:link
	{
		text-decoration:none;
	}
</style>
</style>


<header >
	<h2 style="color: white;  line-height: 200px;">Cuenta</h2>
</header>

<section class="box">
  <div class="row">
		<div class="12u">
      <form class="" action="reporte_cliente.php" method="post">
        <div class="select-wrapper">
          <select name="cliente">
            <?php

            $rtn = $bd->clientes();
            foreach ($rtn as $row) {
              echo '<option value="'.$row['id_cliente'].'">'.$row['nombre_cliente'].'</option>';
            }
            ?>

          </select>
      </div>
      <br>

        <button type="submit" class="button" name="btnIngresar" >Mostrar</button>

      </form>
		</div>
  </div>


	<div class="row">
		<div class="6u">
      <form class="" action="reporte_cliente_pdf.php" method="post">
        <input type="hidden" name="id_cliente" value="<?php echo $cliente ?>">
        <button type="submit" class="button" name="" >PDF</button>
      </form>

		</div>
		<div class="6u" style="text-align: right;">
			<a href='salidas.php'><img src="images/plus.png"></a>
		</div>

	</div>

	<div class="row">
		<div class="12u">
			<div class="table-wrapper">
				<table class="alt" id="salidas">
					<thead style="display:table-row-group;">
						<tr>
							<th>Folio</th>
							<th>Total</th>
							<th>Saldo</th>
							<th>Abono</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tfoot style="display: table-header-group;">
						<tr>
              <th>Folio</th>
							<th>Total</th>
							<th>Saldo</th>
							<th>Abono</th>
							<th>Fecha</th>
						</tr>
					</tfoot>
					<tbody>

						<?php
						foreach ($res as $row) {
							echo ('<tr>');
							echo('<td>'.$row['id_venta'].'</td>');
							echo('<td>'.$row['total'].'</td>');
							echo('<td>'.$row['saldo'].'</td>');
							echo('<td>'.$row['abono'].'</td>');
							echo('<td>'.$row['fecha_abono'].'</td>');
							echo ('</tr>');
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>



</html>
