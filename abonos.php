<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Onconational GDL - Entradas de producto</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
	<?php require 'Controlador/bdd.php';
        $db = new bdd();
if (isset($_SESSION["tipo4"])==true || isset($_SESSION["tipo1"])==true){
    $fechaG=$_SESSION["UltimoAcceso"];
    $actual = date("Y-n-j H:i:s");
    $tiempo=(strtotime($actual)-strtotime($fechaG));
    if ($tiempo >=900) {
        header('Location: Controlador/logOut.php');
    }
    else{
        $_SESSION["UltimoAcceso"]=$actual;
    }

$form='					<form class="form" id="salidaglobal" role="form"  name="salidaglobal" method="POST">
									
										<div class="row uniform 50%" >
											<div class="4u(mobilep)">
												<label>Folio de venta: </label><input type="text" name="folio_venta" id="folio_venta" value="" required="" placeholder="" />
											</div>
										</div>
										<div class="row uniform 50%" >
											<div class="4u(mobilep)">
												<label>Monto a abonar: </label><input type="text" name="abono" id="abono" value="" required="" placeholder="" />
											</div>
										</div>
										
										<div class="row">
										
											<div class="3u 6u(narrower) 12u$(mobilep)" align="center">
											
												<br/>
												<div align="left">
												<button type="submit" class="button" name=btnAbonar>Abonar</button>
												</div>
											</div>
										</div>
										
									</form>

									<hr />';

				if(isset($_POST['btnAbonar'])){
			date_default_timezone_set('America/Mexico_City');
			$folio_venta=$_POST["folio_venta"];
			$abono=$_POST["abono"];
			$fecha = date("Y-n-j H:i:s");
			
			$get_toab=$db->get_toab($folio_venta);
			
			foreach ($get_toab as $key) {   
			                $saldo=$key['saldo'];
			                $abo=$key['abono'];
			                }
			 if ($abono>$saldo) {
			 	$cambio=$abono-$saldo;
			 	$new_saldo=0;
			 	$new_abono=$abo+$abono-$cambio;
			 	$db->up_abonosabs($new_saldo,$new_abono,$fecha,$folio_venta);

			 	$form='		
					 	<h2>CAMBIO: $ '.$cambio.'</h2><br/>
					 	<h2>SALDO: $ '.$new_saldo.'</h2><br/>
									<ul class="actions">
										<li><a href="indexpagos.php" class="button alt">Terminar</a></li>
										
									</ul>
									<hr />';
			 }
			 else{
			 	$new_abono=$abono;
			 	$new_saldo=$saldo-$abono;
			 	$cambio=0;
			 	$db->up_abonosabs($new_saldo,$new_abono,$fecha,$folio_venta);

			 		 	$form='		
					 	<h2>CAMBIO: $ '.$cambio.'</h2><br/>
					 	<h2>SALDO: $ '.$new_saldo.'</h2><br/>
									<ul class="actions">
										<li><a href="indexpagos.php" class="button alt">Terminar</a></li>
										
									</ul>
									<hr />';

			 }

			
			}
			              	
    ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1><a href="indexalmacen.php">OnconationalGDL</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="indexalmacen.php">Inicio</a></li>
							
							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<section class="box">
				<header>
						<h2>Abonos de cuentas</h2>
					</header>
									
					<?php echo $form; ?>


								</section>


			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
				  <?php
        }
        else{
             header('Location: index.php');
             echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
        }
        ?>

	</body>
</html>
