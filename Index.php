<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
  <head>
<?php require 'Controlador/bdd.php';
$bd = new bdd();
if (isset($_SESSION["tipo1"]) == true) {
  header('Location: indexadmin.php');
}
if (isset($_SESSION["tipo2"]) == true) {
  header('Location: indexalmacen.php');
}
if (isset($_SESSION["tipo3"]) == true) {
  header('Location: indexventas.php');
}
if (isset($_SESSION["tipo4"]) == true) {
  header('Location: indexpagos.php');

} else {
  $form = '   <h4>Inicio de sesión</h4>
                <form method="post" action="#">
                    <div class="row uniform 50%">
                      <div class="6u 12u(mobilep)">
                        <input type="text" name="usuario" id="usuario" value="" required="" placeholder="Usuario" />
                      </div>
                      <div class="6u 12u(mobilep)">
                        <input type="password" name="pass" id="pass" value="" required="" placeholder="Contraseña" />
                      </div>
                    </div>

                    <div class="row">

                      <div class="3u 6u(narrower) 12u$(mobilep)" align="center">

                        <br/>
                        <div align="left">
                        <button type="submit" class="button special fit" name=btnLogIn>Entrar</button>
                        </div>
                      </div>
                    </div>

                  </form>';
  if (isset($_POST['btnLogIn'])) {
    $usu  = $_POST['usuario'];
    $pass = $_POST['pass'];
    $rtn  = $bd->ini_sesion($usu, $pass);
    $size = sizeof($rtn);
    if ($size == 0) {
      echo ('<script type="text/javascript"> alert("Datos incorrectos, vuelva a insertar");</script>');
    } else {
      foreach ($rtn as $key) {
        $_SESSION["cargo"] = $key['cargo'];
        $_SESSION["usr"]   = $key["nombre_usuario"];
      }
      if ($_SESSION["cargo"] == 'ADMINISTRADOR') {
        $_SESSION['tipo1']        = true;
        $_SESSION["UltimoAcceso"] = date("Y-n-j H:i:s");
        header("Location: indexadmin.php");

      }
      if ($_SESSION["cargo"] == 'ALMACEN') {
        $_SESSION['tipo2']        = true;
        $_SESSION["UltimoAcceso"] = date("Y-n-j H:i:s");
        header("Location: indexalmacen.php");

      }
      if ($_SESSION["cargo"] == 'VENTAS') {
        $_SESSION['tipo3']        = true;
        $_SESSION["UltimoAcceso"] = date("Y-n-j H:i:s");
        header("Location: indexventas.php");

      }
      if ($_SESSION["cargo"] == 'PAGOS') {
        $_SESSION['tipo4']        = true;
        $_SESSION["UltimoAcceso"] = date("Y-n-j H:i:s");
        header("Location: indexpagos.php");

      }

    }
  }
}
?>
    <title>iCattle Raising</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
  </head>
  <body class="landing">
    <div id="page-wrapper">

      <!-- Header -->
        <header id="header" class="alt">
          <h1><a href="index.html"></a></h1>
          <nav id="nav">
            <ul>


            </ul>
          </nav>
        </header>

      <!-- Banner -->

          <div class="12u">
          <section id="banner">
          <img src="images/logo.png" border="1" alt="" width="300" height="300">
        </section>
        </div>

              <!-- Main -->
        <div id="main" class="container">

            <section class="box">
        <?php echo $form ?>
            </section>

        </div>
      <!-- Footer -->
    </div>
    <!-- Scripts -->
  </body>
</html>