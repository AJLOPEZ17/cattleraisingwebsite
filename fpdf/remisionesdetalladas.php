<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Onconational GDL - Salidas de producto</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body >

	<?php
	require 'Controlador/bdd.php';
	$db = new bdd();
	if (isset($_SESSION["tipo2"])==true || isset($_SESSION["tipo1"])==true){
		if (isset($_SESSION["ID_REM"])==true){
		$fechaG=$_SESSION["UltimoAcceso"];
		$actual = date("Y-n-j H:i:s");
		$tiempo=(strtotime($actual)-strtotime($fechaG));
		$_SESSION["PIEZAS"]=0;
		if ($tiempo >=900) {
			header('Location: Controlador/logOut.php');
		}
		else{
			$_SESSION["UltimoAcceso"]=$actual;

		}

	?>
		<div id="page-wrapper">

			<!-- Header -->
			<header id="header">
				<h1><a href="indexalmacen.php">OnconationalGDL</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="indexalmacen.php">Inicio</a></li>

						<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
					</ul>
				</nav>
			</header>

			<!-- Main -->
			<section class="box">
				<header>
					<h2>Remisiones de productos</h2>
				</header>

				<h1>Producto</h1>


				<form class="form" id="salidadetallada" role="form"  name="salidadetallada" action="Controlador/create_rd.php" method="POST">
					<div class="row uniform 50%" >
						<div class="4u(mobilep)">
							<input type="text" name="piezas" id="piezas" value="" required="" placeholder="Piezas" />
						</div>
					</div>
					<br>
					<div class="row uniform 50%" >
						<div class="4u(mobilep)">
							<div class="select-wrapper">
								<select name="nombreprod">
									<?php
									$rtn = $db->productos();
									foreach ($rtn as $row) {
										echo '<option value="'.$row['nombre_comercial'].'">'.$row['nombre_comercial'].'</option>';
									}
									?>
								</select>
						</div>
					</div>
					</div>

					<div class="row uniform 50%" >
						<div class="4u(mobilep)">
							<input type="text" name="lote" id="lote" value="" required="" placeholder="Lote" />
						</div>
					</div>


					<br>
					<div class="row">
						<div class="3u 6u(narrower) 12u$(mobilep)" align="center">
							<div align="left">
								<button type="submit" class="button" name="btnIngresar" >Ingresar</button>
							</div>
						</div>
					</div>
				</form>

				<hr />
				<?php
				$rem=$db->getactual_remision($_SESSION["ID_REM"]);
				$size=sizeof($rem);
				if ($size==0) {
					$form='<h2>Aún no agrega ningún producto a la remisión</h2>';
				}
				else{

				$form='					<div class="12u">

							<!-- Table -->

									<h3>PRODUCTOS AGREGADOS</h3>


									<div class="table-wrapper" align="right">
										<table>
											<thead>
												<tr>
													<th>PRODUCTO</th>
													<th>LOTE</th>
												</tr>
											</thead><tbody>';
				foreach ($rem as $key) {

$form=$form.'
												<tr>
													<td>'.$key["nombre_producto"].'</td>
													<td>'.$key["lote"].'</td>
												</tr>';
											}
$form=$form.'
											</tbody>
											<tfoot>
												<tr>

												</tr>
											</tfoot>
										</table>
									</div>';

							}
				?>
				<div class="box" align="right">
				<?php echo $form;?>
				</div>

				<form name="form" >
  <input type="submit" name="terminar" value="Terminar" />
</form>

<?php
if (isset($_REQUEST['terminar']))
{

header("location: indexalmacen.php");
}
?>

			</section>


			<!-- Footer -->
			<footer id="footer">
				<ul class="icons">
					<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
					<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
					<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
					<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
				</ul>
				<ul class="copyright">
					<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
				</ul>
			</footer>

		</div>

		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/jquery.scrollgress.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/util.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
		<script src="assets/js/main.js"></script>
<?php
}
}

	else{
		header('Location: index.php');
		echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
	}
?>
</body>
</html>
