<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Onconational GDL - Entradas de producto</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
</head>
<body>
	<div>

		<?php require 'Controlador/bdd.php';
		$bd = new bdd();


		if (isset($_SESSION["tipo2"])==true || isset($_SESSION["tipo1"])==true){
			if(isset($_SESSION["ID_ENTRADA"])==true){
				if(isset($_SESSION["PIEZAS"])==true){
					$fechaG=$_SESSION["UltimoAcceso"];
					$actual = date("Y-n-j H:i:s");
					$tiempo=(strtotime($actual)-strtotime($fechaG));
					if ($tiempo >=900) {
						header('Location: Controlador/logOut.php');
					}
					else{
						$_SESSION["UltimoAcceso"]=$actual;
					}

					$piezas=$_SESSION["PIEZAS"];


					?>
					<div id="page-wrapper">

						<!-- Header -->
						<header id="header">
							<h1><a href="indexalmacen.php">OnconationalGDL</a></h1>
							<nav id="nav">
								<ul>
									<li><a href="indexalmacen.php">Inicio</a></li>

									<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
								</ul>
							</nav>
						</header>

						<!-- Main -->
						<section class="box">
							<header>
								<h2>Entradas de producto</h2>
							</header>

							<ul class="actions">
								<?php
								if($piezas!=0)
								{
									echo '<form name="form1"><input onclick="multiplicarInputs()" id="boton"; type="button" value="Agregar '.$piezas.' producto(s)" /></form>';
								}

								?>

								<?php
								if($piezas==0)
								{
									$total=$bd->gettotal($_SESSION["ID_ENTRADA"]);
									foreach ($total as $key) {
										$tot=$key['total'];
									}
									echo '

									<h2>TOTAL A PAGAR AL PROVEEDOR: $ '.$tot.'</h2><br/>
									<ul class="actions">
										<li><a href="indexalmacen.php" class="button alt">Terminar</a></li>
								</ul>
								<hr />';
							}

							?>
						</ul>
						<li style="list-style: none;"><div id="divMultiInputs"></div>
						</li>


					</section>


					<!-- Footer -->
					<footer id="footer">
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
						</ul>
						<ul class="copyright">
							<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

				</div>

				<!-- Scripts -->
				<script src="assets/js/jquery.min.js"></script>
				<script src="assets/js/jquery.dropotron.min.js"></script>
				<script src="assets/js/jquery.scrollgress.min.js"></script>
				<script src="assets/js/skel.min.js"></script>
				<script src="assets/js/util.js"></script>
				<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
				<script src="assets/js/main.js"></script>
				<?php
			}}}
			else{
				header('Location: index.php');
				echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
			}
			$p=1;

			?>

		</body>



		<script type="text/javascript">
			function multiplicarInputs(text){
				var num= "<?php echo $piezas; ?>"
				var div='';
				var boton= document.getElementById("boton");
				boton.style.visibility="hidden";
				boton.style.height="0px";
				if (num !=0){

					div+='<form class="form" id="entradadetallada" role="form"  name="entradadetallada" method="POST" action="Controlador/create_ed.php"><div class="row uniform 50%" ><div class="6u 12u">Número de piezas<div class="select-wrapper"><select name="n_piezas"><?php
					for($i=1; $i<=$piezas; $i++)
					{
						echo('<option value="'.$i.'">'.$i.'</option>');
					}

					?></select><br></div></div></div><div class="6u 12u">Producto<div class="select-wrapper"><select name="nombreprod"><?php
					$rtn = $bd->productos();
					foreach ($rtn as $row) {
						echo('<option value="'.$row['nombre_comercial'].'">'.$row['nombre_comercial'].'</option>');
					}


					?></select><br></div>Proveedor<div class="select-wrapper"><select name="nombre-proveedor"><?php
					$rtn = $bd->proveedores();
					foreach ($rtn as $row) {
						echo('<option value="'.$row['id_proveedor'].'">'.$row['nombre_proveedor'].'</option>');
					}


					?></select></div></div><div class="row uniform 50%" ><div class="4u(mobilep)"><br><label>Caducidad:</label><input type="date" name="caducidad" id="caducidad" value="" required="" /></div></div><div class="row uniform 50%" ><div class="4u(mobilep)"><br><input type="barcode" name="serial" id="serial" value="" required="" placeholder="Serial" /></div></div><div class="row uniform 50%" ><div class="4u(mobilep)"><br><input type="text" name="lote" id="lote" value="" required="" placeholder="Lote" /><div class="row uniform 50%" ><div class="4u(mobilep)"><br><input type="text" name="costo" id="costo" value="" required="" placeholder="Costo" /></div></div><div class="row"><div class="3u 6u(narrower) 12u$(mobilep)" align="center"><br/><div align="left"><button type="submit" class="button" name=btnIngresar2>Ingresar</button></div></div></div></form><hr />';

				}

				document.getElementById("divMultiInputs").innerHTML=div;
			}
		</script>

		</html>
