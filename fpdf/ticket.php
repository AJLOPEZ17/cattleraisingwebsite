<?php
session_start();
require('fpdf/fpdf.php');
require 'Controlador/bdd.php';
$bd = new bdd();

$suma= $bd->sumatotal_salidas($_SESSION["ID_SALIDA"]);


class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('images/logo.jpeg',10,10,33);
    // Arial bold 15
    $this->Cell(150, 30,'',0);
    $this->SetFont('Arial', '', 11);
    $this->Ln(10);
     $this->Cell(150, 10,'');
    $this->Cell(50, 10, 'Fecha: '.date('d-m-Y').'', 0);
    $this->Ln(30);

    $this->Cell(65, 10,'');
    $this->SetFont('Arial','B',15);
    // Título
    $this->Cell('',10,'Onconational SA de CV');
    $this->Ln(8);
    $this->Cell(80, 10,'');
    $this->SetFont('Arial','',9);
    $this->Cell('',10,'Flores Magon #135');
    $this->Ln(5);
    $this->Cell(78, 10,'');
    $this->Cell('',10,'Col. Cantera Colorada');
    $this->Ln(5);
    $this->Cell(70, 10,'');
    $this->Cell('',10,'San Pedro, Tlaquepaque Jalisco');
    $this->Ln(5);
    $this->Cell(74, 10,'');
    $this->Cell('',10,'CP.45630 Tel:(33)31211443');

}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(23);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(15, 8, 'Piezas', 0);
$pdf->Cell(60, 8, 'Nombre', 0);
$pdf->Cell(40, 8, 'Serie', 0);
$pdf->Cell(45, 8, 'Folio de venta', 0);
$pdf->Cell(25, 8, 'Precio', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA

$productos = $bd->getultima_salida($_SESSION["ID_SALIDA"]);

$item = 0;
$totaluni = 0;
$totaldis = 0;
foreach ($productos as $productos2){
    $item = $item+1;
    $totaluni = $totaluni + 1;
    $totaldis = $totaldis + 1;
    $pdf->Cell(15, 8, '1', 0);
    $pdf->Cell(60, 8,$productos2['nombre_producto'], 0);
    $pdf->Cell(40, 8, 'AS', 0);
    $pdf->Cell(45, 8, $productos2['folio'], 0);
    $pdf->Cell(25, 8, '$ '.$productos2['precio'].'.00', 0);
    $pdf->Ln(8);
}
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115,8,'',0);

$pdf->Cell(47,8,'Total:',0);
$pdf->Cell(20,8,'$'.$suma[0]['precio'].'',0);
$pdf->Output();

?>
