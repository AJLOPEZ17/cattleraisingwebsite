<?php
session_start();
include 'menuadmin.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#entradas tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );

    // DataTable
    var table = $('#entradas').DataTable({
    	"language": {
    		"sProcessing":     "Procesando...",
    		"sLengthMenu":     "Mostrar _MENU_ registros",
    		"sZeroRecords":    "No se encontraron resultados",
    		"sEmptyTable":     "Ningún dato disponible en esta tabla",
    		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    		"sInfoPostFix":    "",
    		"sSearch":         "Buscar:",
    		"sUrl":            "",
    		"sInfoThousands":  ",",
    		"sLoadingRecords": "Cargando...",
    		"oPaginate": {
    			"sFirst":    "Primero",
    			"sLast":     "Último",
    			"sNext":     "Siguiente",
    			"sPrevious": "Anterior"
    		},
    		"oAria": {
    			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
    			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
    		}
    	}
    } );

    // Apply the search
    table.columns().every( function () {
    	var that = this;

    	$( 'input', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    } );
} );
</script>


</head>

<style>
	header{
		text-align: center;
		width: 100%;
		height: 200px;
		background: url(images/pic04.jpg);
		background-size: cover;
	}
	a:link
	{
		text-decoration:none;
	}
</style>
</style>
<?php require 'Controlador/bdd.php';
$bd = new bdd();
$rtn = $bd->ver_abonos();

?>

<header >
	<h2 style="color: white;  line-height: 200px;">Abonos</h2>
</header>

<section class="box">
	<div class="row">
		<div class="6u">
			<h4>Abonos</h4>
		</div>
		<div class="6u" style="text-align: right;">
			<a href='entradas.php'><img src="images/plus.png"></a>
		</div>
	</div>

	<div class="row">
		<div class="12u">
			<div class="table-wrapper">
				<table class="alt" id="entradas">
					<thead style="display:table-row-group;">
						<tr>
							<th>Cliente</th>
							<th>Folio</th>
							<th>Total</th>
							<th>Saldo</th>
							<th>Abono</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tfoot style="display: table-header-group;">
						<tr>
							<th>Cliente</th>
							<th>Folio</th>
							<th>Total</th>
							<th>Saldo</th>
							<th>Abono</th>
							<th>Fecha</th>
						</tr>
					</tfoot>

					<tbody>

						<?php
						foreach ($rtn as $row) {
							echo ('<tr>');
							echo('<td>'.$row['nombre_cliente'].'</td>');
							echo('<td>'.$row['serie'].'   -  '.+ $row['folio'].'</td>');
							echo('<td>$'.$row['total'].'</td>');
							echo('<td>$'.$row['saldo'].'</td>');
							echo('<td>$'.$row['abono'].'</td>');
							echo('<td>'.$row['fecha'].'</td>');



							echo ('</tr>');
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>



</html>
