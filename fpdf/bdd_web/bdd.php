<?php
class bdd {
    private $cnn;
    public function __construct()
            {
        $this->cnn = new PDO('mysql:host=50.62.209.8:3306;dbname=OnconationalGDL',"Onco","OncoGDL0808");
            }
                     public function ini_sesion($usu, $pass){

                $ini_s = $this->cnn->prepare("SELECT * FROM `usuarios` where nombre_usuario = '$usu' and contrasena = '$pass'");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }


/*-------------------------------------------------------------ENTRADAS------------------------------------------------------------------------------------------ */



            public function ver_entradas(){

                $ini_s = $this->cnn->prepare("SELECT * FROM  entradas");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
              public function entrada_global($fecha,$piezas,$serie,$folio,$total,$nombre_usuario)
            {
                $ent = $this->cnn->prepare("INSERT INTO `entradas` (`id_entrada`, `fecha_entrada`, `piezas`, `serie`, `folio`, `total`,`nombre_usuario`) VALUES (NULL,'$fecha', $piezas, '$serie', $folio, $total,'$nombre_usuario')");
                $ent->execute();
            }
             public function entrada_detallada($fecha,$nombre,$caducidad,$serial,$serie, $folio, $proveedor,$costo,$precio)
            {
                $entd = $this->cnn->prepare("INSERT INTO `entradas_detalladas` (`id_entdetalle`, `fecha_entrada`, `nombre`, `caducidad`, `serial`, `serie`, `folio`, `id_proveedor`,`costo`,`precio`) VALUES (NULL,'$fecha','$nombre','$caducidad', '$serial', '$serie', '$folio', '$proveedor',$costo, $precio);");
                $entd->execute();
            }
            public function getid_eg($fecha){

                $getid = $this->cnn->prepare("SELECT `id_entrada`,`piezas` FROM `entradas` where fecha_entrada = '$fecha'");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }

             public function up_folio($id){
                $upf= $this->cnn->prepare("UPDATE `entradas` SET `folio`= $id WHERE `id_entrada` = $id");
                $upf->execute();
            }
            public function sumatotal_entradas($id_entrada){

                $getid = $this->cnn->prepare("SELECT SUM(`costo`) AS costo FROM `entradas_detalladas` where `folio` = $id_entrada");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function up_totalentradas($total,$id_entrada)
            {
                $entd = $this->cnn->prepare("UPDATE `entradas` SET  `total`='$total' where id_entrada=$id_entrada");
                $entd->execute();

            }


            public function entrada_inventario($entradas,$existencia, $id_producto)
            {
                $entd = $this->cnn->prepare("UPDATE `inventario` SET  `entradas`='$entradas',  `existencia`= '$existencia' where id_producto='$id_producto' ");
                $entd->execute();

            }
              public function entrada_inventariodetallado($fecha,$id_prod,$serial,$caducidad,$cantidad,$costo,$precio)
            {
                $entd = $this->cnn->prepare("INSERT INTO `inventario_detallado` (`id`, `fecha_entrada`, `id_producto`,`serial`, `lote`, `caducidad`, `cantidad`,`contenido`,`costo`,`precio`) VALUES (NULL,'$fecha',$id_prod,'$serial', '', '$caducidad',$cantidad,'',$costo, $precio);");
                $entd->execute();
            }
             public function existencia($id_prod){

                $getid = $this->cnn->prepare("SELECT SUM(`cantidad`) AS cantidad FROM `inventario_detallado` where `id_producto` = $id_prod");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function gettotal($id_entrada){

                $getid = $this->cnn->prepare("SELECT * FROM `entradas` where id_entrada=$id_entrada LIMIT 1");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }




/*-------------------------------------------------------------SALIDAS------------------------------------------------------------------------------------------ */



            public function getid_sg($fecha){

                $getid = $this->cnn->prepare("SELECT * FROM `ventas` where fecha_venta = '$fecha'");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function up_foliosalida($id){
                $upf= $this->cnn->prepare("UPDATE `ventas` SET `folio`= $id WHERE `id_venta` = $id");
                $upf->execute();
            }
             public function getactual_salida($id_venta){

                $getid = $this->cnn->prepare("SELECT ventas_detalladas.nombre AS nombre_producto,ventas_detalladas.folio AS folio,ventas_detalladas.serial AS serie_prod,ventas_detalladas.precio AS precio,clientes.nombre_cliente AS nombre_cliente FROM `ventas_detalladas` INNER JOIN `clientes` ON ventas_detalladas.id_cliente=clientes.id_cliente where ventas_detalladas.folio=$id_venta ORDER BY fecha_salida DESC  ");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function getultima_salida($id_venta)
             {
               $getid = $this->cnn->prepare("SELECT ventas_detalladas.lote as lote, ventas.serie AS serie,ventas.folio AS folio, ventas_detalladas.nombre AS nombre_producto,ventas_detalladas.precio AS precio FROM `ventas_detalladas`  INNER JOIN `ventas` ON ventas_detalladas.id_salida = ventas.folio  where ventas_detalladas.id_salida ='$id_venta' ORDER BY fecha_venta DESC");
               $getid->execute();
               $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
               return $rtn;
            }
              public function gettotalcliente_salida($id_venta){

                $getid = $this->cnn->prepare("SELECT ventas.total AS total, clientes.nombre_cliente AS nombre_cliente FROM `ventas` INNER JOIN `clientes` ON ventas.id_cliente=clientes.id_cliente where id_venta = $id_venta");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function up_pagotiposaldo($pago,$saldo,$id){
                $upf= $this->cnn->prepare("UPDATE `ventas` SET `pago`= $pago, `tipo_pago`='', `saldo`=$saldo WHERE `id_venta` = $id");
                $upf->execute();
            }
            public function ver_salidas(){

                $ini_s = $this->cnn->prepare("SELECT * FROM ventas");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }

            public function salida_global($id_cliente,$piezas,$fecha,$serie,$folio,$total)
            {
                $sal = $this->cnn->prepare("INSERT INTO `ventas`(`id_venta`, `id_cliente`, `piezas`, `fecha_venta`, `utilidad`, `saldo`, `status`, `serie`, `folio`, `total`,`pago`,`tipo_pago`) VALUES (NULL,$id_cliente,$piezas,'$fecha',0,0,0,'$serie',$folio,$total,0,0)");
                $sal->execute();
            }

            public function salida_detallada($fecha,$nombre,$serie,$serial, $folio, $cantidad,$precio,$id_cliente)
            {
                $entd = $this->cnn->prepare("INSERT INTO `ventas_detalladas`(`fecha_salida`, `nombre`, `serie`, `serial`, `folio`, `cantidad`, `precio`, `id_cliente`, `id_saldetalle`) VALUES ('$fecha','$nombre','$serie','$serial',$folio,$cantidad,$precio,'$id_cliente',NULL)");
                $entd->execute();

            }
             public function sumatotal_salidas($id_salida){

                $getid = $this->cnn->prepare("SELECT SUM(`precio`) AS precio FROM `ventas_detalladas` where `folio` = $id_salida");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function ticket($id_salida){

               $getid = $this->cnn->prepare("SELECT * FROM `ventas_detalladas` WHERE id_salida=$id_salida");
               $getid->execute();
               $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
               return $rtn;
           }
             public function up_totalsalidas($total,$id_salida)
            {
                $entd = $this->cnn->prepare("UPDATE `ventas` SET  `total`=$total where id_venta=$id_salida");
                $entd->execute();

            }



/*-------------------------------------------------------------INVENTARIO---------------------------------------------------------------------------------------- */


             public function salida_inventario($salidas,$existencia, $id_producto)
            {
                $entd = $this->cnn->prepare("UPDATE `inventario` SET  `salidas`='$salidas',  `existencia`= '$existencia' where id_producto='$id_producto' ");
                $entd->execute();

            }
             public function getpieza_salidas($id_producto){

                $getid = $this->cnn->prepare("SELECT * FROM `inventario_detallado` where id_producto=$id_producto And cantidad=1 ORDER BY fecha_entrada DESC LIMIT 1");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function up_cantidadpieza($id){
                $upp= $this->cnn->prepare("UPDATE `inventario_detallado` SET `cantidad`= 0 WHERE `id` = $id");
                $upp->execute();
            }
             public function inventario($producto){

                $num = $this->cnn->prepare("SELECT * FROM `inventario` INNER JOIN productos on productos.id_producto=inventario.id_producto where nombre_comercial = '$producto'");
                $num->execute();
                $rtn = $num->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }

/*-------------------------------------------------------------ABONOS---------------------------------------------------------------------------------------- */



            public function abonos($id_cliente,$id_venta)
            {
                $abono = $this->cnn->prepare("INSERT INTO `abonos_cliente`(`id_abono`, `id_cliente`, `id_venta`, `total`, `saldo`, `abono`,`fecha_abono`) VALUES (NULL,$id_cliente,$id_venta,0,0,0,'')");
                $abono->execute();
            }

             public function up_abonosts($total,$saldo,$id_venta)
            {
                $entd = $this->cnn->prepare("UPDATE `abonos_cliente` SET `total`=$total,`saldo`=$saldo WHERE id_venta=$id_venta ");
                $entd->execute();

            }

               public function get_toab($folio_venta){

                $gettoab = $this->cnn->prepare("SELECT * FROM `abonos_cliente` where id_venta =$folio_venta");
                $gettoab->execute();
                $rtn = $gettoab->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }

             public function up_abonosabs($saldo,$abono,$fecha,$folio_venta)
            {
                $entd = $this->cnn->prepare("UPDATE `abonos_cliente` SET `saldo`=$saldo,`abono`=$abono,`fecha_abono`='$fecha' WHERE id_venta=$folio_venta ");
                $entd->execute();

            }



/*-------------------------------------------------------------REMISIONES---------------------------------------------------------------------------------------- */

            public function getid_rem($fecha){

                $getid = $this->cnn->prepare("SELECT * FROM `remisiones` where fecha_salida = '$fecha'");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function up_folioremision($id){
                $upf= $this->cnn->prepare("UPDATE `remisiones` SET `folio`= $id WHERE `id_remision` = $id");
                $upf->execute();
            }
             public function remision_global($id_usuario,$piezas,$fecha,$serie,$folio,$total)
            {
                $sal = $this->cnn->prepare("INSERT INTO `remisiones`(`id_remision`, `id_usuario`, `piezas`, `fecha_salida`,`serie`, `folio`, `total`) VALUES (NULL,$id_usuario,$piezas,'$fecha','$serie',$folio,$total)");
                $sal->execute();
            }
            public function remision_detallada($fecha,$nombre,$serie,$serial, $folio, $cantidad,$precio,$id_usuario)
            {
                $entd = $this->cnn->prepare("INSERT INTO `remisiones_detalladas`(`fecha_salida`, `nombre`, `serie`, `serial`, `folio`, `cantidad`, `precio`, `id_usuario`, `id_salida`) VALUES ('$fecha','$nombre','$serie','$serial',$folio,$cantidad,$precio,'$id_usuario',NULL)");
                $entd->execute();

            }
              public function sumatotal_remisiones($id_salida){

                $getid = $this->cnn->prepare("SELECT SUM(`precio`) AS precio FROM `remisiones_detalladas` where `folio` = $id_salida");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function up_totalremisiones($total,$id_salida)
            {
                $entd = $this->cnn->prepare("UPDATE `remisiones` SET  `total`=$total where id_remision=$id_salida");
                $entd->execute();

            }
                 public function getactual_remision($id_venta){

                $getid = $this->cnn->prepare("SELECT remisiones_detalladas.nombre AS nombre_producto,remisiones_detalladas.folio AS folio,remisiones_detalladas.serial AS serie_prod,remisiones_detalladas.precio AS precio,usuarios.nombre_usuario AS nombre_usuario FROM `remisiones_detalladas` INNER JOIN `usuarios` ON remisiones_detalladas.id_usuario=usuarios.id_usuario where remisiones_detalladas.folio=$id_venta ORDER BY fecha_salida DESC  ");
                $getid->execute();
                $rtn = $getid->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }

/*-------------------------------------------------------------OTROS---------------------------------------------------------------------------------------- */



           public function clientes(){
                $upf= $this->cnn->prepare("SELECT * FROM `clientes`");
                $upf->execute();
                $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function proveedores(){
                $upf= $this->cnn->prepare("SELECT * FROM `proveedores`");
                $upf->execute();
                $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function productos(){
                $upf= $this->cnn->prepare("SELECT * FROM `productos`");
                $upf->execute();
                $rtn = $upf->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function ver_inventario(){

                $ini_s = $this->cnn->prepare("SELECT productos.nombre_comercial AS nombre_comercial, productos.nombre_formula AS nombre_formula, inventario.existencia AS existencia, productos.precio AS precio from inventario inner join productos on inventario.id_producto=productos.id_producto");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
            public function ver_productos(){

                $ini_s = $this->cnn->prepare("SELECT * FROM  productos");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function ver_remisiones(){

                $ini_s = $this->cnn->prepare("SELECT * FROM  remisiones");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }
             public function ver_abonos(){

                $ini_s = $this->cnn->prepare("SELECT c.nombre_cliente AS nombre_cliente,v.serie AS serie,v.folio AS folio,ac.total AS total,ac.saldo AS saldo,ac.abono AS abono,ac.fecha_abono AS fecha FROM `abonos_cliente` ac INNER JOIN `clientes` c ON ac.id_cliente = c.id_cliente INNER JOIN `ventas` v ON ac.id_venta = v.id_venta");
                $ini_s->execute();
                $rtn = $ini_s->fetchAll(PDO::FETCH_ASSOC);
                return $rtn;
            }




     }
?>
