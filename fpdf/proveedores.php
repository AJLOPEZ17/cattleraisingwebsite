<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Onconational GDL - Gastos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
	<?php require 'Controlador/bdd.php';
        $db = new bdd();
if (isset($_SESSION["tipo2"])==true || isset($_SESSION["tipo1"])==true){
    $fechaG=$_SESSION["UltimoAcceso"];
    $actual = date("Y-n-j H:i:s");
    $tiempo=(strtotime($actual)-strtotime($fechaG));
    if ($tiempo >=900) {
        header('Location: Controlador/logOut.php');
    }
    else{
        $_SESSION["UltimoAcceso"]=$actual;
    }


    ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1><a href="indexalmacen.php">OnconationalGDL</a></h1>
					<nav id="nav">
						<ul>
							<?php if (isset($_SESSION["tipo2"])==true) { ?>
							<li><a href="indexalmacen.php">Inicio</a></li>
							<?php
							}
							if (isset($_SESSION["tipo1"])==true) { ?>
							<li><a href="indexadmin.php">Inicio</a></li>
							<?php
			                }
							?>

							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<section class="box">
				<header>
						<h2>Proveedores</h2>
					</header>
									
					<form class="form" id="proveedor" role="form"  name="gastos" action="Controlador/create_proveedores.php" method="POST">
										<div class="row uniform 50%">
											<div class="6u 12u(mobilep)">
											<label>Nombre del proveedor:</label><input type="text" name="nombre_p" id="nombre_p" value="" placeholder="" />
											</div>
											</div>

										<div class="row">

											<div class="3u 6u(narrower) 12u$(mobilep)" align="center">

												<br/>
												<div align="left">
												<button type="submit" class="button" name=btnAgregar>Agregar</button>
												</div>
											</div>
										</div>

									</form>



								</section>


			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
						<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
						<li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
				  <?php
        }
        else{
             header('Location: index.php');
             echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
        }
        ?>

	</body>
</html>
