<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Onconational GDL - Reportes de ventas</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
	<?php require 'Controlador/bdd.php';
        $db = new bdd();
if (isset($_SESSION["tipo1"])==true){
    $fechaG=$_SESSION["UltimoAcceso"];
    $actual = date("Y-n-j H:i:s");
    $tiempo=(strtotime($actual)-strtotime($fechaG));
    if ($tiempo >=900) {
        header('Location: Controlador/logOut.php');
    }
    else{
        $_SESSION["UltimoAcceso"]=$actual;
    }

$form='					<form class="form" role="form" action="reportegastospdf.php" name="reporte" method="POST">

										<div class="row uniform 50%" >
											<div class="4u(mobilep)">
											<label>Fecha de inicio</label>
												<input type="date" name="inicio" required="" /><br><br>

											<label>Fecha final</label>
												<input type="date" name="final" required="" />
											</div>
										</div>
										<div class="row">

											<div class="3u 6u(narrower) 12u$(mobilep)" align="center">

												<br/>
												<div align="left">
												<button type="submit" class="button" name=btnIngresar>Generar</button>
												</div>
											</div>
										</div>

									</form>

									<hr />';

    ?>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1><a href="indexalmacen.php">OnconationalGDL</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="indexadmin.php">Inicio</a></li>

							<li><a href="Controlador/logOut.php">Cerrar Sesi&oacuten</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<section class="box">
				<header>
						<h2>Reportes de gastos</h2>
					</header>

					<?php echo $form; ?>


								</section>


			<!-- Footer -->

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollgress.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
				  <?php
        }
        else{
             header('Location: index.php');
             echo('<script type="text/javascript"> alert("Inicie sesion en su cuenta");</script>');
        }
        ?>

	</body>
</html>
