<?php
session_start();
require('fpdf/fpdf.php');
require 'Controlador/bdd.php';
$bd = new bdd();

$cliente=$_POST["id_cliente"];

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
  $cliente=$_POST["id_cliente"];
    // Logo
    $this->Image('images/logo.jpeg',10,10,33);
    // Arial bold 15
    $this->Cell(150, 30,'',0);
    $this->SetFont('Arial', '', 11);
    $this->Ln(10);
     $this->Cell(150, 10,'');
    $this->Cell(50, 10, 'Fecha: '.date('d-m-Y').'', 0);
    $this->Ln(30);



    $this->Cell(80, 10,'');
    $this->SetFont('Arial','B',15);
    // Título
    $bd = new bdd();

    $res = $bd->nombre_cliente($cliente);
    foreach ($res as $cliente1) {
           $cliente2=$cliente1["nombre_cliente"];
        }
        if($cliente==0)
        {
          $this->Cell('',10,'');
        }else{
    $this->Cell('',10,$cliente2);
  }
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(23);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10, 8, '', 0);
$pdf->Cell(10, 8, 'No.', 0);
$pdf->Cell(40, 8, 'Fecha', 0);
$pdf->Cell(20, 8, 'Total', 0);
$pdf->Cell(20, 8, 'Saldo', 0);
$pdf->Cell(20, 8, 'Abono', 0);
$pdf->Cell(20, 8, 'Folio', 0);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
//CONSULTA

$res = $bd->cuenta_cliente($cliente);

$item = 0;
$totaluni = 0;
$totaldis = 0;
foreach ($res as $datos)
{
    $item = $item+1;
    $pdf->Cell(10, 8, '', 0);
    $pdf->Cell(10, 8, $item, 0);
    $pdf->Cell(40, 8,$datos['fecha_abono'], 0);
    $pdf->Cell(20, 8, '$'.$datos['total'].'.00', 0);
    $pdf->Cell(20, 8, '$'.$datos['saldo'].'.00', 0);
    $pdf->Cell(20, 8, '$'.$datos['abono'].'.00', 0);
    $pdf->Cell(20, 8, $datos['id_abono'], 0);
    $pdf->Ln(8);
}
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(10,8,'',0);
$pdf->Cell(10,8,'',0);
$pdf->Cell(20,8,'',0);
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(115,8,'',0);

$pdf->Output();

?>
