<?php
session_start();
include 'menuadmin.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#inventario tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );

    // DataTable
    var table = $('#inventario').DataTable({
            "language": {
                 "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        } );

    // Apply the search
    table.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
	</script>


</head>

<style>
	header{
		text-align: center;
		width: 100%;
		height: 200px;
		background: url(images/pic04.jpg);
		background-size: cover;
	}
	a:link
	{
		text-decoration:none;
	}
</style>
</style>
<?php require 'Controlador/bdd.php';
$bd  = new bdd();
$rtn = $bd->ver_inventario();

?>

<header >
	<h2 style="color: white;  line-height: 200px;">Inventario</h2>
</header>

<section class="box">
	<div class="row">
		<div class="6u">
			<a href="inventario_pdf.php" style="margin-top:200px; right:0" target="_blank"><img src="images/pdf.png" width="50px" height="50px"></a>
		</div>
		<div class="12u">
			<div class="table-wrapper">
				<table class="alt" id="inventario">
					<thead style="display:table-row-group;">
						<tr>
							<th>Marca</th>
							<th>Sexo</th>
							<th>Existencia</th>
							<th>Tipo</th>
						</tr>
					</thead>
					 <tfoot style="display: table-header-group;">
            <tr>
							<th>Marca</th>
							<th>Sexo</th>
							<th>Existencia</th>
							<th>Tipo</th>
            </tr>
        </tfoot>
					<tbody>

						<?php
foreach ($rtn as $row) {
 echo ('<tr>');
 echo ('<td>' . $row['marca'] . '</td>');
 echo ('<td>' . $row['sexo'] . '</td>');
 echo ('<td>' . $row['existencia'] . '</td>');
 echo ('<td> ' . $row['tipo'] . '</td>');
 echo ('</tr>');
}
?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>



</html>
